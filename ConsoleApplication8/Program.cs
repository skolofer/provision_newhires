﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.DirectoryServices;
using System.Net;
using System.Threading;
using System.IO;
using Newtonsoft.Json;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.PhantomJS;
using Freshbooks.Library.Model;
using RestSharp;

namespace New_Users
{
    class Program
    {
        public const String CONSUMER_KEY = "";
        public const String CONSUMER_SECRET = "";
        public static readonly String OAUTHTOKEN = "9ba4502eab7dedb26401f9506a2c42a7";
        private static StaffMembersRequest staffMembersRequest;

        static void Main(string[] args)
        {
            //Variables
            Console.Title = "Provision System Access";
            Console.WriteLine("What is the new Display Name? (Ex: John Doe)");
            string DisplayName = Console.ReadLine();
            Console.WriteLine("Please Wait");
            string UserName = DisplayName.Replace(" ", ".").ToLower();
            String[] NameSplit = DisplayName.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
            string EmailAddress = string.Concat(UserName + "@gomural.com").ToLower();
            string directory = @"C:\NewUsers\";

            string ADresult = CreateADUser(UserName, EmailAddress, DisplayName);
            Console.WriteLine(ADresult);

            var LastPassResult = CreateLastPass(DisplayName, EmailAddress);
            Console.WriteLine(LastPassResult);

            var WIWResult = CreateWIW(DisplayName, EmailAddress, NameSplit);
            Console.WriteLine(WIWResult);

            string PBXResult = CreatePBXExt(DisplayName);
            Console.WriteLine(PBXResult);

            //string FBResults = CreateFreshBooks(DisplayName, EmailAddress); //**In Prog
            //Console.WriteLine(FBResults);


            LogResults(directory, DisplayName, ADresult, LastPassResult, WIWResult, PBXResult);
            Console.ReadLine();
            Environment.Exit(0);
        }

        static void LogResults(string directory, string DisplayName, string ADresult, string LastPassResult, string WIWResult, string PBXResult)
        {
            directory = string.Concat(directory + "Log.txt");



            //Logging PBX Results
            if(PBXResult.Contains("FAILED"))
            {
                string PBX = PBXResult;
                using (StreamWriter Log = new StreamWriter(directory, true))
                {
                    Log.WriteLine(DisplayName);
                    Log.WriteLine(ADresult);
                    Log.WriteLine(LastPassResult);
                    Log.WriteLine(WIWResult);
                    Log.WriteLine(PBX);
                    Log.WriteLine();
                }
            }

            else
            {
                string PBX1 = string.Concat("PBX Extension: " + PBXResult);
                string pbx2 = string.Concat("PBX Password: maax" + PBXResult);
                using (StreamWriter Log = new StreamWriter(directory, true))
                {
                    Log.WriteLine(DisplayName);
                    Log.WriteLine(ADresult);
                    Log.WriteLine(LastPassResult);
                    Log.WriteLine(WIWResult);
                    Log.WriteLine(PBX1);
                    Log.WriteLine(pbx2);
                    Log.WriteLine();
                }
            }
        }

        static string CreateADUser(string UserName, string EmailAddress, string DisplayName)
        {
            //Variables
            string Password = "NewMuralUser101!";
            string strPath = "LDAP://go.mural:389/OU=Mural_Users,DC=go,DC=mural";
            string oGUID = string.Empty;
            String[] Name = DisplayName.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
            string StrUser = string.Concat("CN=" + UserName);

            //Creating AD User
            DirectoryEntry dirEntry = new DirectoryEntry(strPath);
            DirectoryEntry newUser = dirEntry.Children.Add("CN=" + DisplayName, "user");
            newUser.Properties["samAccountName"].Add(UserName);
            newUser.Properties["displayName"].Add(DisplayName);
            newUser.Properties["mail"].Add(EmailAddress);
            newUser.Properties["givenName"].Add(Name[0]);
            newUser.Properties["sn"].Add(Name[1]);
            newUser.CommitChanges();
            oGUID = newUser.Guid.ToString();
            newUser.Invoke("SetPassword", new object[] { Password });
            newUser.Properties["userAccountControl"].Value = 0x200;
            newUser.CommitChanges();
            dirEntry.Close();
            newUser.Close();
            string ADresult = "Successfully Created AD User";
            return ADresult;
        }

        static string CreateLastPass(string DisplayName, string EmailAddress)
        {
            //Configuring API Request
            var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://lastpass.com/enterpriseapi.php");
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream());
            var extra = new { username = EmailAddress, fullname = DisplayName };
            var vm = new { cid = "8318562", provhash = "d7e254cb7eabf66a8c20cfab487e7a11f1d6af9b73da40128f745fdfd38844ab", cmd = "batchadd", data = extra };

            //Call to delete LastPass User
            //var extra = new { username = EmailAddress, deleteaction = 1 };
            //var vm = new { cid = "8318562", provhash = "d7e254cb7eabf66a8c20cfab487e7a11f1d6af9b73da40128f745fdfd38844ab", cmd = "deluser", data = extra };

            var dataString = Newtonsoft.Json.JsonConvert.SerializeObject(vm);
            streamWriter.Write(dataString);
            streamWriter.Flush();
            streamWriter.Close();

            //Getting Response from API
            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            var streamReader = new StreamReader(httpResponse.GetResponseStream());
            var result = streamReader.ReadToEnd();
            result = Convert.ToString(result);
            result = result.Replace('"', ' ');
            if (result == "{ status : OK }")
            {
                string LPSuccess = "LastPass account created successfully";
                return LPSuccess;
            }
            else
            {
                string LPFail = "Failed creating the LastPass account";
                return LPFail;
            }
        }

        static string CreateFreshBooks(string DisplayName, string EmailAddress)
        {

            ////Configuring API Request
            //var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://muralconsulting.freshbooks.com/api/");
            //httpWebRequest.ContentType = "application/json";
            //httpWebRequest.Method = "POST";
            ////httpWebRequest.Credentials. = string.Concat("michele@gomural.com", "9ba4502eab7dedb26401f9506a2c42a7");
            //httpWebRequest.Headers.Add("Authorization: michele@gomural.com 9ba4502eab7dedb26401f9506a2c42a7");

            ////Sending POST to create LastPass User
            //var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream());
            ////var login = 
            //var vm = new { method = "staff.current" };


            //var dataString = Newtonsoft.Json.JsonConvert.SerializeObject(vm);
            //streamWriter.Write(dataString);
            //streamWriter.Flush();
            //streamWriter.Close();

            ////Getting Response from API
            //var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            //var streamReader = new StreamReader(httpResponse.GetResponseStream());
            //var result = streamReader.ReadToEnd();
            //result = Convert.ToString(result);
            //Console.WriteLine(result);

            return "FU";

        }
        static string CreateWIW(string DisplayName, string EmailAddress, string[] NameSplit)
        {
            //WIW variables
            string wiwurl = "https://app.wheniwork.com/login";
            string wiwUser = "shawn@maaxcloud.com";
            string wiwPW = "F5@xQHO6B%";
            string WIWResults = "";

            //Configure Selenium Settings
            ChromeOptions options = new ChromeOptions();
            options.AddUserProfilePreference("download.default_directory", "C:\\Downloads");
            options.AddUserProfilePreference("intl.accept_languages", "nl");
            options.AddUserProfilePreference("download.prompt_for_download", "false");
            options.AddUserProfilePreference("webdriver.chrome.silentOutput", "true");
            options.AddArguments("--disable-extensions"); ;
            var service = OpenQA.Selenium.PhantomJS.PhantomJSDriverService.CreateDefaultService();
            service.HideCommandPromptWindow = true;

            //Navigate to URL
            PhantomJSDriver driver = new PhantomJSDriver(service);
            driver.Navigate().GoToUrl(wiwurl);
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(20));
            WebDriverWait wait2 = new WebDriverWait(driver, TimeSpan.FromSeconds(15));
            wait.Until(ExpectedConditions.ElementIsVisible(By.Id("login-username")));
            Thread.Sleep(500);

            //Send Username and PW
            driver.FindElement(By.Id("login-username")).SendKeys(wiwUser);
            driver.FindElement(By.Id("login-password")).SendKeys(wiwPW);
            driver.FindElement(By.Id("login-button")).Click();
            wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("clearfix")));

            //Provision New User
            driver.Navigate().GoToUrl("https://mural-consulting.wheniwork.com/employees/");
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//a[@class='button-kit white medium add-employee']")));
            driver.FindElement(By.XPath("//a[@class='button-kit white medium add-employee']")).Click();
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//div[@class='dialog-kit employee-dialog employee-modal-refresh visible']")));
            driver.FindElement(By.XPath("//div[@class='dialog-kit employee-dialog employee-modal-refresh visible']")).Click();

            driver.SwitchTo().ActiveElement();
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//div[@class='dialog-body']//input[@name='first_name']")));
            driver.FindElement(By.XPath("//div[@class='dialog-body']//div[@class='form-text']//input[@name='first_name']")).SendKeys(NameSplit[0]);
            Thread.Sleep(500);
            driver.FindElement(By.XPath("//div[@class='dialog-body']//div[@class='form-text']//input[@name='last_name']")).SendKeys(NameSplit[1]);
            Thread.Sleep(500);
            driver.FindElement(By.XPath("//div[@class='dialog-body']//div[@class='form-text email-text']//input[@name='email']")).SendKeys(EmailAddress);
            Thread.Sleep(500);
            driver.FindElement(By.XPath("//div[@class='dialog-footer cf']//button[@id='edit-employee-save']")).Click();
            Thread.Sleep(2000);

            //Check To see if User Exists
            ICollection<IWebElement> UserCheck = driver.FindElements(By.XPath("//div[@class='column name']//div[@class='user-name']/span"));
            string WIWexception = "0";
            foreach (IWebElement user in UserCheck)
            {
                string UserCheck2;
                try
                {
                    UserCheck2 = user.GetAttribute("title").ToString();
                    if (UserCheck2.Contains(DisplayName))
                    {
                        WIWexception = "1";
                    }
                }
                catch
                {
                }
            }
            if (WIWexception == "1")
            {
                WIWResults = "WIW account created successfully";
            }
            else
            {
                WIWResults = "FAILED creating the WIW Account";
            }
            driver.Quit();
            return WIWResults;
        }

        static string CreatePBXExt(string DisplayName)
        {
            //Declare Variables
            string Username = "admin";
            string Password = "i-6a9cc5eb";
            string pbxurl = "http://pbx.gomural.com/admin/config.php#";
            string exturl = "http://pbx.gomural.com/admin/config.php?display=extensions&tech_hardware=sip_generic";
            string outboundCID = "";
            string extpw;
            string pbxdirectory = @"C:\NewUsers\Extensions\";
            string PBXResult = "";

            if (Directory.Exists(pbxdirectory))
            {
                Directory.Delete(pbxdirectory, true);
            }
            Directory.CreateDirectory(pbxdirectory);

            //Setting Chrome Settings
            ChromeOptions options = new ChromeOptions();
            options.AddUserProfilePreference("download.default_directory", "C:\\NewFolder");
            options.AddUserProfilePreference("intl.accept_languages", "nl");
            options.AddUserProfilePreference("download.prompt_for_download", "false");
            options.AddUserProfilePreference("webdriver.chrome.silentOutput", "true");
            options.AddArgument("--disable-extensions");

            var service = OpenQA.Selenium.PhantomJS.PhantomJSDriverService.CreateDefaultService();
            service.HideCommandPromptWindow = true;

            //Navigate to URL
            PhantomJSDriver driver = new PhantomJSDriver(service);
            driver.Navigate().GoToUrl(pbxurl);
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(25));
            wait.Until(ExpectedConditions.ElementIsVisible(By.Id("login_admin")));
            driver.FindElement(By.Id("login_admin")).Click();
            Thread.Sleep(200);

            driver.SwitchTo().ActiveElement();
            driver.SwitchTo().ActiveElement();
            driver.FindElement(By.XPath("//div[@class='ui-dialog ui-widget ui-widget-content ui-corner-all ui-front ui-dialog-buttons ui-draggable']//input[@name='username']")).SendKeys(Username);
            driver.FindElement(By.XPath("//div[@class='ui-dialog ui-widget ui-widget-content ui-corner-all ui-front ui-dialog-buttons ui-draggable']//input[@name='password']")).SendKeys(Password);
            driver.FindElement(By.XPath("//div[@class='ui-dialog ui-widget ui-widget-content ui-corner-all ui-front ui-dialog-buttons ui-draggable']//span[contains(text(), 'Continue')]")).Click();
            wait.Until(ExpectedConditions.ElementIsVisible(By.Id("page_Main_Overview_overview")));

            //Finding Next EXT
            driver.Navigate().GoToUrl("http://pbx.gomural.com/admin/config.php?display=extensions");
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//div[@class='pull-left pagination-detail']//button[@class='btn btn-default  dropdown-toggle']")));
            driver.FindElement(By.XPath("//div[@class='pull-left pagination-detail']//button[@class='btn btn-default  dropdown-toggle']")).Click();
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//a[contains(text(),'500')]")));
            driver.FindElement(By.XPath("//a[contains(text(),'500')]")).Click();
            Thread.Sleep(1000);

            IWebElement baseTable = driver.FindElement(By.XPath("//div[@class='fixed-table-container']//div[@class='fixed-table-body']//table[@id='table-all']"));
            ICollection<IWebElement> rows = baseTable.FindElements(By.XPath("//tr"));
            ICollection<IWebElement> cells = baseTable.FindElements(By.XPath("//td"));

            int PBXcount = 0;
            foreach (IWebElement cell in cells)
            {
                try
                {
                    string ExtCheck1 = cell.FindElement(By.XPath(string.Concat("//tr[@data-index='" + PBXcount + "']"))).Text.ToString();
                    String[] Ext1 = ExtCheck1.Split(new string[] { "\n" }, StringSplitOptions.RemoveEmptyEntries);
                    string PulledData1 = Ext1[0].ToString();
                    using (StreamWriter ExtensionWriter = new StreamWriter(@"C:\NewUsers\Extensions\Extensions.txt", true))
                    {
                        ExtensionWriter.WriteLine(PulledData1);
                    }
                }
                catch
                {
                }
                PBXcount++;
            }

            var reader = new StreamReader(File.OpenRead(@"C:\NewUsers\Extensions\Extensions.txt"));
            List<string> ExtList = new List<string>();
            while (!reader.EndOfStream)
            {
                var line = reader.ReadLine();
                var values = line.Split(',');
                ExtList.Add(values[0]);
            }
            reader.Close();

            int extcount = 0;
            string extstop = "0";
            try
            {
                foreach (var e in ExtList)
                {
                    if (extstop != "1")
                    {
                        if (ExtList[extcount] == "Extension8001")
                        {
                            int extcount2 = extcount - 2;
                            string NextExtension1 = ExtList[extcount2].ToString().Replace("Extension", "");
                            int NextExtension2 = Int32.Parse(NextExtension1);
                            int NextExtension3 = NextExtension2 + 1;
                            string NextExtension = NextExtension3.ToString();
                            extpw = "maax" + NextExtension;

                            //Change to Ext
                            driver.Navigate().GoToUrl(exturl);
                            Thread.Sleep(200);
                            driver.FindElement(By.Name("extension")).SendKeys(NextExtension);
                            driver.FindElement(By.Name("name")).SendKeys(DisplayName);
                            if (outboundCID != "")
                            {
                                driver.FindElement(By.Name("outboundcid")).SendKeys(outboundCID);
                            }
                            driver.FindElement(By.Name("devinfo_secret")).Clear();
                            driver.FindElement(By.Name("devinfo_secret")).SendKeys(extpw);
                            driver.FindElement(By.Name("userman_assign")).Click();
                            driver.FindElement(By.Name("userman_assign")).SendKeys("n");
                            driver.FindElement(By.Name("userman_assign")).Click();
                            driver.FindElement(By.Name("submit")).Click();
                            Thread.Sleep(3500);

                            string newURL = string.Concat("http://pbx.gomural.com/admin/config.php?display=extensions&extdisplay=" + NextExtension + "#advanced");
                            driver.Navigate().GoToUrl(newURL);
                            Thread.Sleep(2000);
                            wait.Until(ExpectedConditions.ElementIsVisible(By.Name("devinfo_nat")));
                            driver.FindElement(By.Name("devinfo_nat")).Click();
                            driver.FindElement(By.Name("devinfo_nat")).SendKeys("y");
                            driver.FindElement(By.Name("devinfo_nat")).Click();
                            Thread.Sleep(100);
                            driver.FindElement(By.Name("submit")).Click();
                            Thread.Sleep(2500);
                            wait.Until(ExpectedConditions.ElementIsVisible(By.Id("button_reload")));
                            driver.FindElement(By.Id("button_reload")).Click();
                            Thread.Sleep(5000);
                            PBXResult = NextExtension;
                            extstop = "1";
                        }
                    }
                    extcount++;
                }
            }
            catch
            {
                PBXResult = string.Concat("FAILED creating the PBX Extension");
            }
            driver.Quit();
            return PBXResult;
        }
    }
}
